import UIKit

final class NetworkManager {
    
    static let shared = NetworkManager()
    
    //MARK: - Download image
    func putOrDownloadImage(from urlString: String, completition: @escaping (_ image: UIImage) -> ()) {
        if let data = UserDefaults.standard.value(forKey: urlString) as? Data {
            guard let image = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                completition(image)
            }
        } else {
            let session = URLSession.shared
            guard let url = URL(string: urlString) else { return }
            
            session.dataTask(with: url) { data, _, error in
                guard let data = data, error == nil else { return }
                guard let image = UIImage(data: data) else { return }
                
                DispatchQueue.main.async {
                    completition(image)
                }
                
                let imageData = image.pngData()
                UserDefaults.standard.set(imageData, forKey: urlString)
            }.resume()
        }
    }
}
