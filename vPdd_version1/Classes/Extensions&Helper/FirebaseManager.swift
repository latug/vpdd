import Foundation
import Firebase
import CodableFirebase

final class FirebaseManager {
    
    static let shared = FirebaseManager()
    private let databasePath = "Events"
    private let userPath = "Users"
    private lazy var databaseReference = Database.database().reference()
    private lazy var storageReference = Storage.storage().reference()
    private lazy var auth = Auth.auth()
    
    //MARK: - Read information in Firebase Database
    func readFromDatabase(completitionHandler: @escaping (_ event: [Event]) -> Void) {
        let reference = databaseReference.child(databasePath)
        reference.observeSingleEvent(of: .value) { (snapshot) in
            var events = [Event]()
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    if let value = snap.value as? Dictionary<String,AnyObject> {
                        do {
                            let event = try FirebaseDecoder().decode(Event.self, from: value)
                            events.append(event)
                        } catch let error {
                            print(error)
                        }
                    }
                }
            }
            completitionHandler(events.reversed())
        }
    }
    
    //MARK: - Write information from Firebase Database
    func writeToDatabase(of event: Event) {
        let newEvent : Dictionary<String, AnyObject> = [
            "name": event.name as AnyObject,
            "address": event.address as AnyObject,
            "switchEvent": event.switchEvent as AnyObject,
            "numberCar": event.numberCar as AnyObject,
            "description": event.description as AnyObject,
            "url": event.url as AnyObject
        ]

        let reference = databaseReference.child(databasePath)
        reference.childByAutoId().setValue(newEvent)
    }
    
    //MARK: - Upload image in Firebase Storage
    func uploadImageInStorage(image: UIImage, completitionHandler: @escaping (_ imageUrl: String) -> ()) {
        let name = UUID().uuidString
        let reference = storageReference.child(name)
        let resizedImage = image.resizedTo1MB() // convert image to 1 mb size
        
        guard let data = resizedImage?.pngData() else { return }
        
        reference.putData(data, metadata: nil) { (_, error) in
            guard error == nil else { return }
            
            reference.downloadURL{ (url, error) in
                guard let downloadUrl = url else { return }
                guard error == nil else { return }

                completitionHandler(downloadUrl.absoluteString)
            }
        }
    }
    
    //MARK: - Create Account
    func createAccount(with name: String, andWith password: String, andWith email: String) {
        let reference = databaseReference.child(userPath)
        
        auth.createUser(withEmail: email, password: password) { (result, error) in
            guard error != nil, let result = result else { return }
            
            reference.child(result.user.uid).updateChildValues(["name" : name,
                                                                "email" : email,
                                                                "password" : password])
        }
    }
    
    //MARK: - Sing in
    func signIn(with email: String, and password: String, completition: @escaping () -> ()) {
        auth.signIn(withEmail: email, password: password) { (result, error) in
            if error == nil {
                completition()
            }
        }
    }
}
