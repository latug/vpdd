import UIKit
import Foundation

final class CameraManager: NSObject {

    fileprivate var currentVC: UIViewController?
    var imageStorage: ((UIImage) -> Void)?
    
    //MARK: - Method
    func showActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default) { [weak self] (_ ) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let pickerController = UIImagePickerController()
                pickerController.delegate = self
                pickerController.allowsEditing = true
                pickerController.sourceType = .camera
                self?.currentVC?.present(pickerController, animated: true, completion: nil)
            }
        })
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default) { [weak self] (_ ) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let pickerController = UIImagePickerController()
                pickerController.delegate = self
                pickerController.allowsEditing = true
                pickerController.sourceType = .photoLibrary
                self?.currentVC?.present(pickerController, animated: true, completion: nil)
            }
        })
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
}

//MARK: - UIImagePickerControllerDelegate
extension CameraManager: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UINavigationControllerDelegate
extension CameraManager: UINavigationControllerDelegate {
    func imagePickerController(_ picker :UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.imageStorage?(editedImage)
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imageStorage?(originalImage)
        } else {
            print("Something went wrong")
        }
        
        currentVC?.dismiss(animated: true, completion: nil)
    }
}
