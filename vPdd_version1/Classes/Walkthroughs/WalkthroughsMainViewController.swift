import UIKit
import BWWalkthrough

final class WalkthroughsMainViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    private var isShow = true
    
    //MARK: - Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard isShow else { //  needed for a full exit from two controllers
            dismiss(animated: false, completion: nil)
            return
        }
        
        guard let pageOne = R.storyboard.walkthroughs.page1(),
            let pageTwo = R.storyboard.walkthroughs.page2(),
            let pageThree = R.storyboard.walkthroughs.page3(),
            let pageFour = R.storyboard.walkthroughs.page4(),
            let walkthroughs = R.storyboard.walkthroughs.container() else { return }
        
        walkthroughs.delegate = self
        
        walkthroughs.add(viewController: pageOne)
        walkthroughs.add(viewController: pageTwo)
        walkthroughs.add(viewController: pageThree)
        walkthroughs.add(viewController: pageFour)
        
        isShow = false
        present(walkthroughs, animated: false) {
            UserDefaults.standard.set(true, forKey: "shown")
        }
    }
}

extension WalkthroughsMainViewController {
    func walkthroughCloseButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
}
