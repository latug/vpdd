import UIKit
import Firebase

final class AdditionViewController: UIViewController {

    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var caseSelectionSegmentControl: UISegmentedControl!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var numberCarTextField: UITextField!
    @IBOutlet private weak var descriptionTextField: UITextField!
    @IBOutlet private weak var previewImageButton: UIButton!
    @IBOutlet private weak var addressLabel: UILabel!
    
    private var imageUrl: String?
    private var isLoadding = false
    private let activitiIndicator = UIActivityIndicatorView(style: .gray)
    private let firebaseManager = FirebaseManager.shared
    private lazy var cameraManager = CameraManager()
    var selectedAddress: String?
    
    //MARK: - Life cycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isLoadding { // after application "addPhotoButton" shows activitiIndicator
            activitiIndicator.center = addButton.center
            addButton.isHidden = true
            view.addSubview(activitiIndicator)
            activitiIndicator.startAnimating()
        }
        
        if let address = selectedAddress {
            addressLabel.text = address
            addressLabel.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == R.segue.additionViewController.showPreview.identifier {
            guard let destinationVC = segue.destination as? PreviewImageViewController else { return }
            destinationVC.image = previewImageButton.imageView?.image
        }
    }
    
    //MARK: - IBAction
    @IBAction private func tapScreenAction(_ sender: UITapGestureRecognizer) { // hide the keyboard by tapping the screen
        nameTextField.resignFirstResponder()
        numberCarTextField.resignFirstResponder()
        descriptionTextField.resignFirstResponder()
    }
    
    @IBAction private func additingEventAction(_ sender: UIButton) {
        guard caseSelectionSegmentControl.selectedSegmentIndex >= 0,
            let name = nameTextField.text, !name.isEmpty,
            let address = selectedAddress, !address.isEmpty,
            let numberCar = numberCarTextField.text, !numberCar.isEmpty,
            let descriptionEvent = descriptionTextField.text, !descriptionEvent.isEmpty,
            let url = imageUrl, !url.isEmpty else {
                showAlert(title: "Ошибка", message: "Заполните все поля и добавьте изображение")
                return
        }
        
        firebaseManager.writeToDatabase(of: Event(name: name, address: address, switchEvent: caseSelectionSegmentControl.selectedSegmentIndex, numberCar: numberCar, description: descriptionEvent, url: url))
        
        clearAllFields()
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction private func addPhotoAction(_ sender: UIButton) {
        cameraManager.showActionSheet(vc: self)
        
        cameraManager.imageStorage = { [weak self] (image) in
            self?.isLoadding = true
            
            self?.firebaseManager.uploadImageInStorage(image: image) { [weak self] url in
                self?.imageUrl = url
                self?.activitiIndicator.stopAnimating()
                self?.isLoadding = false
                self?.addButton.isHidden = false
            }
            
            self?.previewImageButton.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
            self?.previewImageButton.isHidden = false
        }
    }
    
    //MARK: - Supporting methods
    private func clearAllFields() {
        nameTextField.text = nil
        selectedAddress = nil
        numberCarTextField.text = nil
        descriptionTextField.text = nil
        caseSelectionSegmentControl.selectedSegmentIndex = UISegmentedControl.noSegment
        imageUrl = nil
        previewImageButton.isEnabled = false
        previewImageButton.isHidden = true
        
        view.endEditing(true) // hide keyboard
    }
}
