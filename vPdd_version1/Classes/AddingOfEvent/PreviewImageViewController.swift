//
//  PreviewImageViewController.swift
//  vPdd
//
//  Created by Ilya Latugovskii on 13/09/2019.
//  Copyright © 2019 Ilya. All rights reserved.
//

import UIKit

class PreviewImageViewController: UIViewController {

    @IBOutlet private weak var imageView: UIImageView!
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
    }
}
