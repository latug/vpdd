import UIKit

final class CreateAccountViewController: UIViewController {
    
    @IBOutlet private weak var createButton: UIButton!
    @IBOutlet private weak var loginTextField: UITextField!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var questionLabel: UILabel!
    
    private let firebaseManager = FirebaseManager.shared
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showWalkthroughs()
    }
    
    //MARK: - IBAction
    @IBAction func createOrSignInAccountAction(_ sender: UIButton) {
        guard let name = loginTextField.text, !name.isEmpty,
            let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                showAlert(title: "Ошибка", message: "Заполните все поля")
                return
        }
        
        firebaseManager.createAccount(with: name, andWith: password, andWith: email)
        dismiss(animated: true, completion: nil)
    }
    
    private func showWalkthroughs() {
        let shown = UserDefaults.standard.value(forKey: "shown") as? Bool
        
        if shown == nil { // show Walkthroughs only once
            guard let walkthroughsVC = R.storyboard.walkthroughs().instantiateInitialViewController() else { return }
            present(walkthroughsVC, animated: true, completion: nil)
        }
    }
}
