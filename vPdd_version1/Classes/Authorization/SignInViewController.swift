import UIKit

class SignInViewController: UIViewController {

    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    private let firebaseManager = FirebaseManager.shared
    
    //MARK: - IBAction
    @IBAction func signInAction(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                showAlert(title: "Ошибка", message: "Заполните все поля")
                return
        }
        
        firebaseManager.signIn(with: email, and: password) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
}
