import UIKit

final class ListViewController: UIViewController {
    
    @IBOutlet private weak var eventTableView: UITableView!
    
    private let refreshControl = UIRefreshControl()
    private var events = [Event]()
    private var firebaseManager = FirebaseManager.shared
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        eventTableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        firebaseManager.readFromDatabase { [weak self] (events) in
            self?.events = events
            
            DispatchQueue.main.async {
                self?.eventTableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let destinationVC = segue.destination as? ExtensionEventViewController else { return }
        
        if let indexPath = eventTableView.indexPathForSelectedRow {
            let cell = eventTableView.cellForRow(at: indexPath) as? TableViewCell
            destinationVC.takeValues(from: events[indexPath.row], andFrom: cell?.mainImage)
        }
    }
    
    //MARK: - Supporting methods
    @objc private func refreshData() {
        firebaseManager.readFromDatabase { [weak self] (events) in
            self?.events = events
            
            DispatchQueue.main.async {
                self?.eventTableView.reloadData()
                self?.refreshControl.endRefreshing()
            }
        }
    }
}

//MARK: - UITableViewDataSource
extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
}

//MARK: - UITableViewDelegate
extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = eventTableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.incident.identifier, for: indexPath) as! TableViewCell
        
        let event = events[indexPath.row]
        cell.configure(with: event)
        return cell
    }
}
