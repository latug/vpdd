import UIKit

final class TableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var iconTypeImageView: UIImageView!
    @IBOutlet private weak var countCommentLabel: UILabel!
    @IBOutlet private weak var backView: UIView!
    
    private let networkManager = NetworkManager.shared
    var mainImage: UIImage {
        guard let image = mainImageView.image else { return UIImage() }
        return image
    }
    
    //MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        mainImageView.image = R.image.deer()
    }
    
    //MARK: - Supporting methods
    func configure(with event: Event) {
        nameLabel.text = event.name
        descriptionLabel.text = event.description
        iconTypeImageView.image = event.switchEvent == 1 ? R.image.carCrash() : R.image.deer()
        putImage(from: event.url)
    }
    
    private func putImage(from urlString: String) {
        networkManager.putOrDownloadImage(from: urlString) { [weak self] (image) in
            self?.mainImageView.image = image
        }
    }

    private func configCell() {
        // custom cell
        backgroundColor = .clear
        
        backView.layer.borderWidth = 1
        backView.layer.cornerRadius = 3
        backView.layer.borderColor = UIColor.clear.cgColor
        backView.layer.masksToBounds = true
        
        layer.shadowOpacity = 0.18
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 2
        layer.shadowColor = UIColor.black.cgColor
        layer.masksToBounds = false
    }
}
