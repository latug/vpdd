struct Event: Decodable {
    var name: String
    var address: String
    var switchEvent: Int
    var numberCar : String
    var description: String
    var url: String
}
