import UIKit
import Firebase
import GoogleMaps

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBMvIEPk5d4f4UPbr6rp9KrsDoCXJVoKlA")
        
        FirebaseApp.configure()
        Auth.auth().addStateDidChangeListener { (_, user) in
            if user == nil {
                self.showModalAuth()
            }
        }
        return true
    }
    
    func showModalAuth() {
        guard let newVC = R.storyboard.auth.navigationViewController() else { return }
        window?.rootViewController?.present(newVC, animated: true, completion: nil)
    }
}

