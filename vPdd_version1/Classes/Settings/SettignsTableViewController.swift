import UIKit

final class SettignsTableViewController: UITableViewController {

}

//MARK: - UITableViewDelegate
extension SettignsTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            showAlert(title: "Мобильное приложение фиксации нарушений ПДД и случаев ДТП", message: "Разработчик Латуговскй И.Н.")
        case 2:
            guard let walkthroughsVC = R.storyboard.walkthroughs().instantiateInitialViewController() else { return }
            present(walkthroughsVC, animated: true, completion: nil)
        default:
            break
        }
    }
}
