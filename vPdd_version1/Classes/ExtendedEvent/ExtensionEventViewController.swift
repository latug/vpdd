import UIKit

final class ExtensionEventViewController: UIViewController {

    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var caseTypeImageView: UIImageView!
    @IBOutlet private weak var nameEventLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var numberCarLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    private var eventRepository: Event?
    private var imageRepository: UIImage?
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateInterfaceViaEventVariable()
    }
    
    //MARK: - IBAction
    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Supporting methods
    func takeValues(from event: Event, andFrom image: UIImage?) {
        eventRepository = event
        imageRepository = image
    }
    
    private func updateInterfaceViaEventVariable() {
        guard let event = eventRepository,
            let image = imageRepository else { return }
        
        mainImageView.image = image
        caseTypeImageView.image = event.switchEvent == 1 ? R.image.carCrash() : R.image.deer()
        nameEventLabel.text = event.name
        addressLabel.text = event.address
        numberCarLabel.text = event.numberCar
        descriptionLabel.text = event.description
    }
}
