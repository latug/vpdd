import UIKit
import GoogleMaps

final class MapViewController: UIViewController {
    
    @IBOutlet private weak var mapView: GMSMapView!
    @IBOutlet private weak var addressLabel: UILabel!
    
    //MARK: - Lyfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        guard let destination = navigationController?.visibleViewController as? AdditionViewController  else { return }
        destination.selectedAddress = addressLabel.text
    }
    
    //MARK: - Supporting methods
    private func initialConfig() {
        let camera = GMSCameraPosition.camera(withLatitude: 52.09755, longitude: 23.68775, zoom: 12.0)
        addressLabel.text = ""
        mapView.camera = camera
        mapView.delegate = self
    }
    
    private func reverseGeocodeCoordinate(with coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            self?.addressLabel.text = lines.joined(separator: "\n")
        }
    }
}

//MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let coordinate = position.target
        reverseGeocodeCoordinate(with: coordinate)
    }
}



